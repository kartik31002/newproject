output "id_lb" {
  description = "List of IDs of instances"
  value       = module.ec2.id_lb
}
output "id_cn" {
  description = "List of IDs of instances"
  value       = module.ec2.id_cn
}
output "id_wn" {
  description = "List of IDs of instances"
  value       = module.ec2.id_wn
}

output "public_ip_lb" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.ec2.public_ip_lb
}
output "public_ip_cn" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.ec2.public_ip_cn
}
output "public_ip_wn" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.ec2.public_ip_wn
}

output "instance_count" {
  description = "Number of instances to launch specified as argument to this module"
  value       = var.instance_count
}

output "userdatacn" {
  description = "User data for the cn instance"
  value       = module.ec2.user_data_cn
}
output "userdatawn" {
  description = "User data for the cn instance"
  value       = module.ec2.user_data_wn
}
output "userdatalb" {
  description = "User data for the cn instance"
  value       = module.ec2.user_data_lb
}