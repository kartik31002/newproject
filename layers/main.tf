module "ec2" {
    source = "../modules"


  instance_type                         = var.instance_type
  instance_count                        = var.instance_count
  ami                                   = var.ami
  key_name                              = var.key_name
  name                                  = var.name

}