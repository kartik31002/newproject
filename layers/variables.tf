variable "name" {
  description = "Name to be used on all resources as prefix"
  #default     = "lima"
}

variable "instance_count" {
  description = "Number of instances to launch"
  #default = 1
}

variable "ami" {
  description = "ID of AMI to use for the instance"
  #default     = "ami-002070d43b0a4f171"
}

variable "instance_type" {
  description = "The type of instance to start"
  #default     = "t2.large"
}

variable "key_name" {
  description = "The key name to use for the instance"
  #default     = "terraform"
}

#variable "vpc_security_group_ids"{
#  description = "A list of security group IDs to associate with"
#  default = ["sg-0a3aed1e7778fb606"]
#}