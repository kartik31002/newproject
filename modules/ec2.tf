resource "aws_instance" "lb" {
  ami              = var.ami
  instance_type    = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.eks-d-sg.id]
  tags = {
    "Name" = "lb-${var.name}"
  }

  
  depends_on = [aws_instance.wn]
  user_data = templatefile("./conftemplates/lbconf.tftpl", {"ipaddr" = aws_eip.lbip.public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})

}

resource "aws_instance" "cn" {
  count = var.instance_count
  ami              = var.ami
  instance_type    = var.instance_type
  key_name         = var.key_name
  vpc_security_group_ids = [aws_security_group.eks-d-sg.id]

  tags = {
    "Name" = "cn-${var.name}-${count.index}"
  }

  
  depends_on = [aws_eip.cnip]
  user_data = templatefile("./conftemplates/cnconf.tftpl", {"node_index" = count.index, "ipaddr" = aws_eip.cnip[count.index].public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})

}

resource "aws_instance" "wn" {
  count = var.instance_count
  ami              = var.ami
  instance_type    = var.instance_type
  key_name         = var.key_name
  vpc_security_group_ids = [aws_security_group.eks-d-sg.id]

  tags = {
    "Name" = "wn-${var.name}-${count.index}"
  }

  

  depends_on = [aws_instance.cn]
  user_data = templatefile("./conftemplates/wnconf.tftpl", {"node_index" = count.index, "ipaddr" = aws_eip.wnip[count.index].public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})


}

resource "aws_security_group" "eks-d-sg" {
  name = "${var.name}-sg"
  ingress {
    description      = "ssh"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "http"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "https"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    description      = "haproxy"
    from_port        = 6443
    to_port          = 6443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

#resource "local_file" "cnconf" {
#  depends_on = [aws_eip.cnip, aws_eip.wnip, aws_eip.lbip]
#  count = var.instance_count
#  content = templatefile("./conftemplates/cnconf.tftpl", {"node_index" = count.index, "ipaddr" = aws_eip.cnip[count.index].public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})
#  filename = "./cnconf${count.index}.sh"
#}

#resource "local_file" "wnconf" {
#  depends_on = [aws_eip.cnip, aws_eip.wnip, aws_eip.lbip]
#  count = var.instance_count
#  content = templatefile("./conftemplates/wnconf.tftpl", {"node_index" = count.index, "ipaddr" = aws_eip.wnip[count.index].public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})
#  filename = "./wnconf${count.index}.sh"
#}

resource "local_file" "kubconf" {
  depends_on = [aws_eip.cnip]
  content = templatefile("./conftemplates/kubconf.tftpl", {"ipaddr" = aws_eip.cnip[0].public_ip})
  filename = "./kubconf.sh"
}

resource "aws_eip" "cnip" {
  count = var.instance_count
  domain = "vpc"

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> controlips.txt"
  }
}
resource "aws_eip" "wnip" {
  count = var.instance_count
  domain = "vpc"

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> workerips.txt"
  }
}
resource "aws_eip" "lbip" {
  domain = "vpc"

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> lbips.txt"
  }
}

resource "aws_eip_association" "eip_assoc_cn" {
  count = var.instance_count
  instance_id   = aws_instance.cn[count.index].id
  allocation_id = aws_eip.cnip[count.index].id
}
resource "aws_eip_association" "eip_assoc_wn" {
  count = var.instance_count
  instance_id   = aws_instance.wn[count.index].id
  allocation_id = aws_eip.wnip[count.index].id
}
resource "aws_eip_association" "eip_assoc_lb" {
  instance_id   = aws_instance.lb.id
  allocation_id = aws_eip.lbip.id
}