output "id_lb" {
  description = "List of IDs of instances"
  value       = aws_instance.lb.*.id
}
output "id_cn" {
  description = "List of IDs of instances"
  value       = aws_instance.cn.*.id
}
output "id_wn" {
  description = "List of IDs of instances"
  value       = aws_instance.wn.*.id
}

output "public_ip_lb" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_eip.lbip.public_ip
}
output "public_ip_cn" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_eip.cnip.*.public_ip
}
output "public_ip_wn" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_eip.wnip.*.public_ip
}

output "instance_count" {
  description = "Number of instances to launch specified as argument to this module"
  value       = var.instance_count
}

output "user_data_cn" {
  description = "Userdata for the cn instance"
  value       = templatefile("./conftemplates/cnconf.tftpl", {"node_index" = "0", "ipaddr" = aws_eip.cnip[0].public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})
}
output "user_data_wn" {
  description = "Userdata for the cn instance"
  value       = templatefile("./conftemplates/wnconf.tftpl", {"node_index" = "0", "ipaddr" = aws_eip.wnip[0].public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})
}
output "user_data_lb" {
  description = "Userdata for the cn instance"
  value       = templatefile("./conftemplates/lbconf.tftpl", {"ipaddr" = aws_eip.lbip.public_ip, "cn_ip_addr" = aws_eip.cnip.*.public_ip, "wn_ip_addr" = aws_eip.wnip.*.public_ip, "lb_ipaddr" = aws_eip.lbip.public_ip})
}