################################################
#makefile

STATEBUCKET            = new-terraform-bucket
STATEREGION            = us-east-1
STATEKEY               = terraform/tf.tfstate
LOCATION               = layers

ifndef LOCATION
$(error LOCATION is not set)
endif

.PHONY: plan


first-run:
	@echo "initialize terraform statefile"
	cd $(LOCATION) && \
	rm -r .terraform/ || true && \
	export AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) && \
    export AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) && \
	export TF_VAR_name=$(name) export TF_VAR_ami=$(ami) export TF_VAR_instance_count=$(instance_count) export TF_VAR_instance_type=$(instance_type) export TF_VAR_key_name=$(key_name)

init:
	@echo "initialize terraform statefile and workspace"
	cd $(LOCATION) && \
	export TF_VAR_name=$(name) export TF_VAR_ami=$(ami) export TF_VAR_instance_count=$(instance_count) export TF_VAR_instance_type=$(instance_type) export TF_VAR_key_name=$(key_name) && \
	terraform init -reconfigure -backend-config="bucket=$(STATEBUCKET)" -backend-config="key=$(STATEKEY)" -backend-config="region=$(STATEREGION)"

validate: init
	@echo "running terraform validate"
	cd $(LOCATION) && \
	terraform validate -no-color

plan: validate
	@echo "running terraform plan"
	cd $(LOCATION) && \
	export TF_VAR_name=$(name) export TF_VAR_ami=$(ami) export TF_VAR_instance_count=$(instance_count) export TF_VAR_instance_type=$(instance_type) export TF_VAR_key_name=$(key_name) && \
	terraform plan -no-color

apply: plan
	@echo "running terraform apply"
	cd $(LOCATION) && \
	export TF_VAR_name=$(name) export TF_VAR_ami=$(ami) export TF_VAR_instance_count=$(instance_count) export TF_VAR_instance_type=$(instance_type) export TF_VAR_key_name=$(key_name) && \
	terraform apply  -auto-approve -no-color

plan-destroy: validate
	@echo "running terraform plan-destroy"
	cd $(LOCATION) && \
	export TF_VAR_name=$(name) export TF_VAR_ami=$(ami) export TF_VAR_instance_count=$(instance_count) export TF_VAR_instance_type=$(instance_type) export TF_VAR_key_name=$(key_name) && \
	terraform plan -destroy -no-color

destroy: init
	@echo "running terraform destroy"
	cd $(LOCATION) && \
	export TF_VAR_name=$(name) export TF_VAR_ami=$(ami) export TF_VAR_instance_count=$(instance_count) export TF_VAR_instance_type=$(instance_type) export TF_VAR_key_name=$(key_name) && \
	terraform destroy -auto-approve -no-color

sshcn:
	@echo "ssh into $(IP_SSH)"
	cd $(LOCATION) && \
	chmod 600 terraform.pem && \
	cat cnconf${NODE_INDEX}.sh | ssh -tt -o StrictHostKeyChecking=no -i terraform.pem centos@$(IP_SSH)

sshwn:
	@echo "ssh into $(IP_SSH)"
	cd $(LOCATION) && \
	chmod 600 terraform.pem && \
	cat wnconf${NODE_INDEX}.sh | ssh -tt -o StrictHostKeyChecking=no -i terraform.pem centos@$(IP_SSH)

sshlb:
	@echo "ssh into $(IP_SSH)"
	cd $(LOCATION) && \
	chmod 600 terraform.pem && \
	cat lbconf.sh | ssh -tt -o StrictHostKeyChecking=no -i terraform.pem centos@$(IP_SSH)

sshkub:
	@echo "ssh into $(IP_SSH)"
	cd $(LOCATION) && \
	chmod 600 terraform.pem && \
	cat kubconf.sh | ssh -tt -o StrictHostKeyChecking=no -i terraform.pem centos@$(IP_SSH)